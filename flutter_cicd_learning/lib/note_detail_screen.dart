import 'package:flutter/material.dart';
import 'package:flutter_cicd_learning/main.dart';
import 'package:flutter_cicd_learning/object/note.dart';

class NoteDetailScreen extends StatefulWidget {
  const NoteDetailScreen({this.noteId, Key? key}) : super(key: key);

  final int? noteId;

  @override
  State<NoteDetailScreen> createState() => _NoteDetailScreenState();
}

class _NoteDetailScreenState extends State<NoteDetailScreen> {
  final _noteBox = objectBox.store.box<Note>();
  final _titleController = TextEditingController();
  final _contentController = TextEditingController();

  bool get _isAdd => _crNote == null;
  Note? _crNote;

  @override
  void initState() {
    if (widget.noteId != null) {
      _crNote = _noteBox.get(widget.noteId!);
      if (_crNote != null) {
        _titleController.text = _crNote?.title ?? "";
        _contentController.text = _crNote?.description ?? "";
      }
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_ios_new,
            size: 24,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        actions: [
          IconButton(
            onPressed: () {
              _saveNote();
            },
            icon: const Icon(
              Icons.save,
              color: Colors.white,
              size: 30,
            ),
          ),
        ],
        title: const Text('Note'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: [
            const SizedBox(height: 30),
            TextField(
              controller: _titleController,
              decoration: const InputDecoration(hintText: "Title"),
            ),
            const SizedBox(height: 30),
            TextField(
              controller: _contentController,
              decoration: const InputDecoration(hintText: "Content"),
              maxLines: 5,
            ),
          ],
        ),
      ),
    );
  }

  void _saveNote() {
    final title = _titleController.text;
    final content = _contentController.text;
    if (title.isEmpty || content.isEmpty) return;

    if (_isAdd) {
      final newNote = Note()
        ..title = title
        ..description = content
        ..createdDate = DateTime.now();
      _noteBox.put(newNote);
    } else {
      _crNote!.title = title;
      _crNote!.description = content;
      _noteBox.put(_crNote!);
    }

    Navigator.of(context).pop();
  }
}
