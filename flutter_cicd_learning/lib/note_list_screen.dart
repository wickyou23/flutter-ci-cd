import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_cicd_learning/main.dart';
import 'package:flutter_cicd_learning/note_detail_screen.dart';
import 'package:flutter_cicd_learning/object/note.dart';
import 'package:flutter_cicd_learning/objectbox.g.dart';

class NoteListScreen extends StatefulWidget {
  const NoteListScreen({Key? key}) : super(key: key);

  @override
  State<NoteListScreen> createState() => _NoteListScreenState();
}

class _NoteListScreenState extends State<NoteListScreen> {
  final _noteBox = objectBox.store.box<Note>();

  late List<Note> _allNotes = [];
  late StreamSubscription _noteSub;

  @override
  void dispose() {
    _noteSub.cancel();
    super.dispose();
  }

  @override
  void initState() {
    final query = _noteBox.query()
      ..order(Note_.createdDate, flags: Order.descending);
    Stream<Query<Note>> watchedQuery = query.watch(triggerImmediately: true);
    _noteSub = watchedQuery.listen((event) {
      setState(() {
        _allNotes = event.find();
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_ios_new,
            size: 24,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) {
                    return const NoteDetailScreen();
                  },
                  settings: const RouteSettings(name: "Note_Detail_Screen"),
                ),
              );
            },
            icon: const Icon(
              Icons.add_circle_rounded,
              color: Colors.white,
              size: 30,
            ),
          )
        ],
        title: const Text('Notes'),
      ),
      body: _allNotes.isEmpty
          ? const Center(
              child: Text("No note found."),
            )
          : ListView.separated(
              itemBuilder: (mContext, index) {
                final item = _allNotes[index];
                return Container(
                  color: index.isEven ? Colors.grey[300] : Colors.grey[100],
                  child: TextButton(
                    onPressed: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) {
                            return NoteDetailScreen(noteId: item.id);
                          },
                          settings:
                              const RouteSettings(name: "Note_Detail_Screen"),
                        ),
                      );
                    },
                    style: ButtonStyle(
                      padding:
                          MaterialStateProperty.all(const EdgeInsets.all(16)),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                item.title ?? "N/A",
                                style: Theme.of(context).textTheme.headline5,
                              ),
                              const SizedBox(height: 8),
                              Text(
                                item.description ?? "N/A",
                                style: Theme.of(context).textTheme.bodyText1,
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(width: 8),
                        IconButton(
                          onPressed: () {
                            _removeNote(_allNotes[index]);
                          },
                          icon: const Icon(
                            Icons.remove_circle,
                            color: Colors.redAccent,
                            size: 30,
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (mContext, index) {
                return const Divider(
                  height: 1.0,
                  color: Colors.grey,
                );
              },
              itemCount: _allNotes.length,
              padding: const EdgeInsets.all(20),
            ),
    );
  }

  void _removeNote(Note note) {
    _noteBox.remove(note.id);
  }
}
