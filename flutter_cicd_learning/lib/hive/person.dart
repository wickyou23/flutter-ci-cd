import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hive/hive.dart';

part 'person.g.dart';
part 'person.freezed.dart';

@unfreezed
@HiveType(typeId: 0)
class Person extends HiveObject with _$Person {
  factory Person({
    @HiveField(0) String? name,
    @HiveField(1) int? age,
    @HiveField(2) List<Person>? friends,
  }) = _Person;

  Person._();

  factory Person.fromJson(Map<String, dynamic> json) => _$PersonFromJson(json);
}
