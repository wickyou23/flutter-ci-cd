# This file contains the fastlane.tools configuration
# You can find the documentation at https://docs.fastlane.tools
#
# For a list of all available actions, check out
#
#     https://docs.fastlane.tools/actions
#
# For a list of all available plugins, check out
#
#     https://docs.fastlane.tools/plugins/available-plugins
#

# Uncomment the line if you want fastlane to automatically update itself
# update_fastlane

default_platform(:android)

platform :android do
  desc "Build apk and upload to AppCenter"
  lane :beta do
    # Change new version for git tags
    change_version_from_git_tags()

    # Build APK
    Dir.chdir("../..") do
      sh("flutter clean")
      sh("flutter pub get")
      sh("flutter build apk -v")
    end

    # Upload to AppCenter
    if File.exist?(File.dirname(__FILE__) + "./../build/app/outputs/apk/release/app-release.apk")
      appcenter_upload(
        api_token: "539f45dca2aebe8867fac9c8ecd8d33b75ff7114",
        owner_name: "lienthang9121991-gmail.com",
        app_name: "FlutterCiCDLearning",
        apk: "../build/app/outputs/apk/release/app-release.apk",
      )
    else
      UI.user_error!("Unable to find apk")
    end
  end

  lane :change_version_from_git_tags do
    regex_version = /^(version: )(\d+\.\d+\.\d+)(\+)(\d+)$/
    Dir.chdir("../..") do
      UI.message("Get version number for git tag")
      version_tag = sh("./get_version_by_tags.sh --android", log: false).split('|', 2)
      if version_tag.nil? || version_tag.length() == 0
        UI.error(">>>>> Cannot get version number from git tag <<<<<")
        UI.message(">>>>> Build the app with current version <<<<<")
        next
      end

      version_name = version_tag[0]
      version_number = version_tag[1]
      full_version_str = "version: #{version_name}+#{version_number}"
      if !(full_version_str =~ regex_version)
        UI.error(">>>>> Version number is invalid <<<<<")
        UI.message(">>>>> Build the app with current version <<<<<")
        next
      end

      content_yaml_file = File.read("pubspec.yaml")
      replace_version = content_yaml_file.gsub!(regex_version, full_version_str)
      File.open("pubspec.yaml", "w") { |file| file << replace_version }
      UI.message(">>>>> Build the app with new version from git tags <<<<<")
    end
  end
end
