import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:objectbox/objectbox.dart';

part 'note.g.dart';
part 'note.freezed.dart';

@unfreezed
class Note with _$Note {
  @Entity(realClass: Note)
  factory Note({
    @Default(0) int id,
    String? title,
    String? description,
    DateTime? createdDate,
  }) = _Note;

  Note._();

  factory Note.fromJson(Map<String, dynamic> json) => _$NoteFromJson(json);
}
