import 'package:flutter/material.dart';
import 'package:flutter_cicd_learning/hive/person.dart';
import 'package:hive_flutter/hive_flutter.dart';

class PersonScreen extends StatefulWidget {
  const PersonScreen({Key? key}) : super(key: key);

  @override
  State<PersonScreen> createState() => _PersonScreenState();
}

class _PersonScreenState extends State<PersonScreen> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _ageController = TextEditingController();

  final Box<Person> _personBox = Hive.box<Person>("persons");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_ios_new,
            size: 24,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: const Text('Persons'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: TextField(
                    controller: _nameController,
                    decoration: const InputDecoration(hintText: "Name"),
                  ),
                ),
                const SizedBox(width: 30),
                Flexible(
                  child: TextField(
                    controller: _ageController,
                    decoration: const InputDecoration(hintText: "Age"),
                  ),
                ),
                const SizedBox(width: 30),
                SizedBox(
                  width: 80,
                  child: ElevatedButton(
                    onPressed: () {
                      if (_nameController.text.isEmpty ||
                          _ageController.text.isEmpty) return;
                      final newPerson = Person(
                        name: _nameController.text,
                        age: int.parse(_ageController.text),
                      );

                      _personBox.add(newPerson);
                      _nameController.text = "";
                      _ageController.text = "";
                    },
                    child: const Text('Save'),
                  ),
                )
              ],
            ),
            const SizedBox(height: 30),
            ValueListenableBuilder(
              valueListenable: _personBox.listenable(),
              builder: (ctx, box, widget) {
                final pBox = box as Box<Person>;
                return Expanded(
                  child: ListView.separated(
                    itemBuilder: (ctx, idx) {
                      final item = pBox.getAt(idx)!;
                      return Container(
                        color: idx.isEven ? Colors.grey[300] : Colors.grey[100],
                        padding: const EdgeInsets.all(10),
                        child: Row(
                          children: [
                            Flexible(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Text("Name: ${item.name}"),
                                  const SizedBox(height: 4),
                                  Text("Age: ${item.age}")
                                ],
                              ),
                            ),
                            IconButton(
                              onPressed: () {
                                _personBox.deleteAt(idx);
                              },
                              icon: const Icon(
                                Icons.remove_circle,
                                color: Colors.red,
                              ),
                            )
                          ],
                        ),
                      );
                    },
                    separatorBuilder: (ctx, idx) {
                      return Divider(
                        height: 1,
                        color: Colors.grey[300]!,
                      );
                    },
                    itemCount: box.values.length,
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
