import 'package:flutter/material.dart';
import 'package:flutter_cicd_learning/hive/person.dart';
import 'package:flutter_cicd_learning/note_list_screen.dart';
import 'package:flutter_cicd_learning/objectbox_manager.dart';
import 'package:flutter_cicd_learning/person_screen.dart';
import 'package:hive_flutter/hive_flutter.dart';

late ObjectBox objectBox;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  //For ObjectBox Store
  objectBox = await ObjectBox.create();

  //For Hive Store
  await Hive.initFlutter();

  Hive.registerAdapter(PersonAdapter());

  await Hive.openBox('testData');
  await Hive.openBox<Person>("persons");

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late Box testDataBox;

  @override
  void initState() {
    super.initState();
    testDataBox = Hive.box('testData');
  }

  void _incrementCounter() {
    final oldVal = testDataBox.get('increment', defaultValue: 0);
    testDataBox.put('increment', oldVal + 1);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            ValueListenableBuilder<Box>(
              valueListenable: testDataBox.listenable(),
              builder: (context, box, widget) {
                return Text(
                  '${box.get('increment', defaultValue: 0)}',
                  style: Theme.of(context).textTheme.headline4,
                );
              },
            ),
            const SizedBox(height: 100),
            ElevatedButton(
              onPressed: (() {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) {
                      return const PersonScreen();
                    },
                    settings: const RouteSettings(name: "Persion_Screen"),
                  ),
                );
              }),
              child: const Text('Person Screen (HiveBox)'),
            ),
            const SizedBox(height: 50),
            ElevatedButton(
              onPressed: (() {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) {
                      return const NoteListScreen();
                    },
                    settings: const RouteSettings(name: "Note_List_Screen"),
                  ),
                );
              }),
              child: const Text('Note List Screen (ObjectBox)'),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}
