ANDROID_TAG="flutter_android_beta"
IOS_TAG="flutter_ios_beta"
FLUTTER_TYPE=""

if [[ $# == 1 ]] ;
then
    for i in $*
    do
        case "$i" in
        "--ios") FLUTTER_TYPE="ios";;
        "--android") FLUTTER_TYPE="android";;
        esac
        break
    done
fi

if ! [[ -z "$FLUTTER_TYPE" ]] ; 
then
    GIT_TAGS=`git tag --contains HEAD`
    for i in $GIT_TAGS
    do
        IS_IOS_TAG=`[[ "$FLUTTER_TYPE" == "ios" ]] && [[ $i == "$IOS_TAG"* ]]`
        IS_ANDROID_TAG=`[[ "$FLUTTER_TYPE" == "android" ]] && [[ $i == "$ANDROID_TAG"* ]]`
        if [[ IS_IOS_TAG || IS_ANDROID_TAG ]] ; then
            FLUTTER_VERSION_NAME=`echo "$i" | cut -d "_" -f 4`
            FLUTTER_VERSION_NUMBER=`echo "$i" | cut -d "_" -f 5`
            break
        fi
    done
fi

if ! [[ -z "$FLUTTER_VERSION_NAME" ]] ;
then
    if [[ -z "$FLUTTER_VERSION_NUMBER" ]] ; then
        FLUTTER_VERSION_NUMBER=1
    fi

    echo "$FLUTTER_VERSION_NAME|$FLUTTER_VERSION_NUMBER" | tr -d '\n'
else
    echo "" | tr -d '\n'
fi
